import React from 'react'
import Card from '../components/Card'

const Favorites = ({ favoriteItems, onAddToLiked }) => {
    console.log(favoriteItems)
    return (
        <div className="content">
            <div className="d-flex justify-between align-center mb-30">
                <h1>Favorites</h1>
            </div>
            <div className="cardWrapper">
                {
                    
                    favoriteItems
                    .map((obj, index) => (
                    <Card
                        key={obj.id}
                        favorited={true}
                        onLiked={onAddToLiked}
                        {...obj}
                        />
                    ))
                }
            </div>
        </div>
    )
}

export default Favorites
