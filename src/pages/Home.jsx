import Card from '../components/Card';

const Home = ({searchValue, onChangeSearchInput, items, onAddToCart, onAddToLiked}) => {
    return (
        <div className="content">
            <div className="d-flex justify-between align-center mb-30">
                <h1>{searchValue ? `Search: ${searchValue}` : 'All'}</h1>
                <div className="searchBlock">
                    <img src="/img/search-icon.svg" alt="Search" />
                    <input onChange={onChangeSearchInput} value={searchValue} type="text" placeholder="Search..." /> 
                </div>
            </div>
            <div className="cardWrapper">
                {
                    items
                    .filter((item) => item.title.toLowerCase().includes(searchValue.toLowerCase()))
                    .map((obj, index) => (
                    <Card
                        key={obj.id}
                        title={obj.title}
                        price={obj.price}
                        image={obj.imageUrl}
                        id={obj.id}
                        onPlus={(product) => onAddToCart(product)}
                        onLiked={(product) => onAddToLiked(product)}
                        />
                    ))
                }
            </div>
      </div>
    )
}

export default Home
