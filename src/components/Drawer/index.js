import React from 'react'

const Drawer = ({onCartClose, onRemoveItem, items = []}) => {
    return (
        <div className="overlay">
        <div className="drawer">
          <div className="drawerHeader d-flex align-center justify-between">
            <h2>Cart</h2>
            <button onClick={onCartClose}>
              <img className="cu-p" src="/img/btn-remove.svg" alt="remove from cart" />
            </button>
          </div>
          <div className="items">
            {items.map((obj) => (
            <div className="cartItem d-flex align-center justify-between">
             <div style={{backgroundImage: `url(${obj.image})`}} className="cartItemImg"></div>
             <div>
                <p>{obj.title}</p>
                <b>{obj.price}</b> 
              </div>
              <img onClick={() => onRemoveItem(obj.id)} src="/img/btn-remove.svg" alt="remove from cart" />
            </div>
            ))}
          </div>
          <div className="cartTotalBlock">
          <ul>
            <li>
              <span>Total price:</span>
              <div></div>
              <b>9 999</b>
            </li>
            <li>
            <span>Tax 5%:</span>
              <div></div>
              <b>99</b>
            </li>
          </ul>
          <button>Apply order</button>
          </div>
        </div>
      </div>
    )
}

export default Drawer
