import {React, useState} from 'react'
import styles from './Card.module.scss'

const Card = ({title, price, id, image, onPlus, onLiked, favorited = false}) => {
    const [isAdded, setIsAdded] = useState(false);
    const [isLiked, setIsLiked] = useState(favorited);

    const onClickPlus = () => {
        onPlus({title, price, image, id})
        setIsAdded(!isAdded);
    }

    const onClickLiked = () => {
        onLiked({id, title, price, image})
        setIsLiked(!isLiked)
    }

    return (
        <div className={styles.card}>
            <div className={styles.favorite}>
            <img onClick={onClickLiked} src={isLiked ? "/img/heart-liked.svg" : "/img/heart-unliked.svg" } alt="unliked" />
            </div>
            <img width={133} height={112} src={image} alt="sneackers" />
            <p>{title}</p>
            <div className={styles.cardBottom}>
            <div className="d-flex flex-column">
                <span>Price:</span>
                <b>{price}</b>
            </div>
            <button onClick={onClickPlus}>
                <img src={isAdded ? "img/checked-btn.svg" : "img/plus-btn.svg"} alt="add to cart" />
            </button>
            </div>
        </div>
    )
}

export default Card
