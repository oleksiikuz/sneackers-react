import {React, useState, useEffect} from 'react';
import { Routes, Route } from 'react-router-dom';
import axios from 'axios';
import Drawer from './components/Drawer';
import Header from './components/Header';
import Home from './pages/Home'
import Favorites from './pages/Favorites';

function App() {
  const [items, setItems] = useState([])
  const [cartOpened, setCartOpened] = useState(false)
  const [cartItems, setCartItems] = useState([])
  const [favoriteItems, setFavoriteItems] = useState([])
  const [searchValue, setSearchValue] = useState('')

  useEffect(() => {
    axios.get('https://61d88ebce6744d0017ba8bca.mockapi.io/items')
      .then((res) => {
        setItems(res.data)
      })
    axios.get('https://61d88ebce6744d0017ba8bca.mockapi.io/cart')
      .then((res) => {
        setCartItems(res.data)
      })
      axios.get('https://61d88ebce6744d0017ba8bca.mockapi.io/favorites')
      .then((res) => {
        setFavoriteItems(res.data)
      })
  }, [])

  const onChangeSearchInput = (event) => {
    setSearchValue(event.target.value)
  }

  const onAddToCart = (product) => {
    axios.post('https://61d88ebce6744d0017ba8bca.mockapi.io/cart', product)
    setCartItems(prev => [...prev, product])
  }

  const onAddToLiked = (product) => {
    console.log(product)
    if (favoriteItems.find(favProduct => favProduct.id === product.id)) {
      axios.delete(`https://61d88ebce6744d0017ba8bca.mockapi.io/favorites/${product.id}`);
      setFavoriteItems(prev => prev.filter(item => item.id !== product.id))
    } else {
      axios.post('https://61d88ebce6744d0017ba8bca.mockapi.io/favorites', product)
      setFavoriteItems(prev => [...prev, product])
    }
  }

  const onRemoveItem = (id) => {
    axios.delete(`https://61d88ebce6744d0017ba8bca.mockapi.io/cart/${id}`)
    setCartItems(prev => prev.filter(item => item.id !== id))
  }

  return (
      <div className="wrapper">
      {cartOpened && <Drawer items={cartItems} onCartClose={() => setCartOpened(false)} onRemoveItem={onRemoveItem} />}
        <Header onCartClick={() => setCartOpened(true)} />
        <Routes>
          <Route path="/" element={
            <Home 
              searchValue={searchValue}
              onChangeSearchInput={onChangeSearchInput}
              items={items}
              onAddToCart={onAddToCart}
              onAddToLiked={onAddToLiked}
            />
          } />
          <Route path="favorites" element={
            <Favorites 
              favoriteItems={favoriteItems}
              onAddToLiked={onAddToLiked}
            />} />
        </Routes>
      </div>
  );
}

export default App;
